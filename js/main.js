window.addEventListener('DOMContentLoaded', function () {
   let acc_main = document.getElementsByClassName('accordion-main'),
       acc_header = document.getElementsByClassName('accordion-header');
   for (let i = 0; i < acc_main.length; i++) {
       acc_header[i].addEventListener('click', function () {
           if (acc_main[i].classList.contains('show')) {
               acc_header[i].classList.remove('open');
           } else {
               acc_header[i].classList.add('open');
           }
       });
   }

});